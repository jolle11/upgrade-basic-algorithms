// Iteracion #1 - Variables

// 1.1
var myFavoriteHero = 'Hulk';

// 1.2
var x = 50;

// 1.3
var h = 5;
var y = 10;

// 1.4
var z = h + y;

// Iteracion #2 - Variables avanzadas

// 1.1
const character = { name: 'Jack Sparrow', age: 10 };
character.age = 25;

//1.2
var firstName = 'Jon';
var lastName = 'Snow';
var age = 24;

console.log(`Soy ${firstName} ${lastName}, tengo ${age} años y me gustan los lobos.`);

// 1.3

const toy1 = { name: 'Buss myYear', price: 19 };
const toy2 = { name: 'Rallo mcKing', price: 29 };

console.log(toy1.price + toy2.price);

// 1.4

let globalBasePrice = 10000;
const car1 = { name: 'BMW m&m', basePrice: 50000, finalPrice: 60000 };
const car2 = { name: 'Chevrolet Corbina', basePrice: 70000, finalPrice: 80000 };

let globalBasePrice = 25000;
car1.finalPrice = car1.basePrice + globalBasePrice;
car2.finalPrice = car2.basePrice + globalBasePrice;

// Iteracion #3 - Operadores

// 1.1
var multiplicacion = 10 * 5;
alert(multiplicacion);

// 1.2
var division = 10 / 2;
alert(division);

// 1.3
var resto = 15 % 9;
alert(resto);

// Vars 1.4, 1.5
var y = 10;
var z = 5;

// 1.4
x4 = y + z;

// 1.5
x5 = y * z;

// Iteracion #4 - Arrays

// 1.1, 1.2, 1.3

// 1.1
const avengers1 = ['HULK', 'SPIDERMAN', 'BLACK PANTHER'];
console.log(avengers[0]);

// 1.2
avengers[0] = 'IRONMAN';

// 1.3
const avengers3 = ['HULK', 'SPIDERMAN', 'BLACK PANTHER'];
alert(avengers.length);

// 1.4
const rickAndMortyCharacters = ['Rick', 'Beth', 'Jerry'];
rickAndMortyCharacters.push('Morty', 'Summer');
console.log(rickAndMortyCharacters[rickAndMortyCharacters.length - 1]);

// 1.5
rickAndMortyCharacters.pop();
console.log(rickAndMortyCharacters[0], rickAndMortyCharacters[rickAndMortyCharacters.length - 1]);

// 1.6
rickAndMortyCharacters.splice(1, 1);
console.log(rickAndMortyCharacters);

// Iteracion #5 - Condicionales

const number1 = 10;
const number2 = 20;
const number3 = 2;

if (number2 / number1 == 2) {
    console.log('number2 dividido entre number1 es igual a 2');
}
if (number1 !== number2) {
    console.log('number1 es estrictamente distinto a number2');
}

if (number3 != number1) {
    console.log('number3 es distinto number1');
}

if (number3 * 5 == number1) {
    console.log('number3 por 5 es igual a number1');
}

if (number3 * 5 == number1 && number1 * 2 == number2) {
    console.log('number3 por 5 es igual a number1 Y number1 por 2 es igual a number2');
}

if (number2 / 2 == number1 || number1 / 5 == number3) {
    console.log('number2 entre 2 es igual a number1 O number1 entre 5 es igual a number3');
}

// Iteracion #6 - Bucles

// 1.1
for (let i = 0; i < 10; i++) {
    console.log(i);
}

// 1.2

for (let i = 0; i < 10; i++) {
    if (i % 2 === 0) {
        console.log(i);
    }
}

// 1.3

for (let i = 0; i < 10; i++) {
    if (i < 9) {
        console.log('Intentando dormir');
    } else {
        console.log('Dormido');
    }
}
